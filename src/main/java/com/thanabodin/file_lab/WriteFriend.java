/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thanabodin.file_lab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class WriteFriend {
    public static void main(String[] args) {
        
        FileOutputStream fos=null;
        try {
            Friend firend1=new Friend("Thidarat",22,"081234567");
            Friend firend2=new Friend("Kodpat",32,"081234567");
            Friend firend3=new Friend("Earn",21,"081234567");
            //friends.dat
            File file =new File ("friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos =new ObjectOutputStream(fos);
            oos.writeObject(firend1);
            oos.writeObject(firend2);
            oos.writeObject(firend3);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
  
}
