/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thanabodin.file_lab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class ReadFriend {
    public static void main(String[] args) {
        FileInputStream fis =null;
        try {
            Friend friend1 =null;
            Friend friend2 =null;
            Friend friend3 =null;
            File file = new File("friends.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            friend1= (Friend) ois.readObject();
            friend2= (Friend) ois.readObject();
            friend3= (Friend) ois.readObject();
            System.out.println(friend1);
            System.out.println(friend2);
            System.out.println(friend3);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
}
