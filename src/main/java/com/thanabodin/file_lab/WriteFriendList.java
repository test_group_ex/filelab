/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thanabodin.file_lab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class WriteFriendList {

    public static void main(String[] args) {
        ArrayList<Friend> friend = new ArrayList<>();
        friend.add(new Friend("Thidarat", 22, "081234567"));
        friend.add(new Friend("Kodpat", 32, "081234567"));
        friend.add(new Friend("Earn", 21, "081234567"));
        
        FileOutputStream fos = null;
        try {

            //friends.dat
            File file = new File("list_of_friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friend);
           
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
